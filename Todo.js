var token;
 function getToken(){
     $.ajax({
         url:'http://localhost:50110/api/TodoStatus/CreateToken',
         type: 'GET',
         dataType: 'json',
         contentType: 'application/json; charset=utf-8',
         success: function( response ){
             token=response.data;
         },
         error: function (error) {
            alert(error);
        }
     });
    } 
   
$(document).ready(function () {
    getToken();
    setTimeout(getAllTodos,100) ;
});  
let newToDo=document.querySelector("#inputtext");
let addToDo=document.querySelector("#btn");
let allToDo=document.querySelector("#todo_tasks");
let removeDiv=document.querySelector("#lists");
let inprogressTasks=document.querySelector("#inprogress_tasks");
let completedTasks=document.querySelector("#completed_tasks");
let todo=[];
var uniqueId;
addToDo.addEventListener('click',()=>{
        if(newToDo.value !=''){
            todo.push(newToDo.value);
        }
        if(newToDo.value==""){
            alert("Please Enter Something");
            return;
        }           
           allToDo.appendChild(generateNewTodo(newToDo.value,uniqueId));
    });
    function generateNewTodo(Item,uniqueId){
        let ListofToDo=document.createElement('div');
        ListofToDo.classList.add('ListofToDo');
        ListofToDo.setAttribute('id', `${uniqueId}`);
       
        let todolist=document.createElement('div');
        todolist.classList.add('todolist');

        ListofToDo.setAttribute("draggable","true");
        ListofToDo.addEventListener("dragstart",dragStart);
        ListofToDo.addEventListener("dragend",dragEnd);

        ListofToDo.appendChild(todolist);
        //Code for creating input text box
        let task_input_element=document.createElement('input');
        task_input_element.classList.add('text');
        task_input_element.type='text';
        task_input_element.value=Item;
        task_input_element.setAttribute("readonly","readonly");

        todolist.appendChild(task_input_element);
        //code for div element
        let task_actions=document.createElement('div');
        task_actions.classList.add('actions');
        //code for creating edit button
        let task_edit=document.createElement('button');
        task_edit.classList.add('edit');
        task_edit.innerHTML='Edit';
        //code for creating delete button
        let task_delete=document.createElement('button');
        task_delete.classList.add('delete');
        task_delete.innerHTML='X';

        task_actions.appendChild(task_edit);
        task_actions.appendChild(task_delete);

        ListofToDo.appendChild(task_actions);
        newToDo.value="";

        task_edit.addEventListener('click',function(){
            var updateId=$(this).parent().parent().attr('id');
            if(task_edit.innerText=='Edit'){
                task_input_element.removeAttribute("readonly");
                task_input_element.focus();
                task_edit.innerText="Update";
            }else{
                task_input_element.setAttribute("readonly","readonly");
                task_edit.innerText="Edit";
                var Val=task_input_element.value;
                let updatedItem=Val;
            var data=JSON.stringify(
                {
                    "Item":updatedItem
                });
                $.ajax( {
                    url: 'http://localhost:50110/api/TodoList/UpdateToDo?id='+`${updateId}`,
                    type: 'PUT',
                    headers: {
                        Authorization : `Bearer ${token}`
                    },
                    data:data,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    success: function( response ) {
                        alert(response);
                    },
                    error: function (error) {
                        alert(error);
                    }
                });
            }         
        });

        task_delete.addEventListener('click', function(){
            var id = $(this).parent().parent().attr('id');
            removeDiv.removeChild.ListofToDo;
            $.ajax({
                url: 'http://localhost:50110/api/TodoList/DeleteToDo?id=' + id,
                type: 'DELETE',
                headers: {
                    Authorization : `Bearer ${token}`
                },
                success: function (response) {
                    alert(response);
                },
                error: function (error) {
                    alert(error);
                }
            });
            location.reload();
           })
        return ListofToDo;   
    }
    const all_Status=document.querySelectorAll('#Todo,#InProgress,#Completed');
    let draggableToDo=null;
    var statusId;
    function dragStart(){
        draggableToDo=this;
        statusId=$(this).attr("id");
    }
    function dragEnd(){
        draggableToDo=null;
    }
    all_Status.forEach((listoftodo)=>{
        listoftodo.addEventListener('dragover',dragOver);
        listoftodo.addEventListener('dragenter',dragEnter);
        listoftodo.addEventListener('dragleave',dragLeave);
        listoftodo.addEventListener('drop',Drop);
    });

    function dragOver(e){
        e.preventDefault();
    }
    function dragEnter(){
    }
    function dragLeave(){
    }
    function Drop(){
        this.appendChild(draggableToDo);
        var Status=$(this).closest('div').attr('id');
        id=statusId;
        var data=JSON.stringify(
            {
                "Status":Status
            });
            $.ajax( {
                url: 'http://localhost:50110/api/TodoStatus/UpdateStatus?id='+`${id}`,
                type: 'PUT',
                headers: {
                    Authorization : `Bearer ${token}`
                },
                data:data,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function( response ) {
                    alert(response);
                },
                error: function (error) {
                    alert(error);
                }
            });
    }   
            var databaseTodos;
            function getAllTodos(){
                    $.ajax( {
                        url: 'http://localhost:50110/api/TodoList/GetAllTodos',
                        type: 'GET',
                        headers: {
                            Authorization : `Bearer ${token}`
                        },
                        dataType: 'json',
                        contentType: 'application/json; charset=utf-8',
                        success: function( response ) {
                            response.filter(response=>{
                                if(response.Status=="Todo"){
                                    var Todos =generateNewTodo(response.Item,response.StatusId)
                                    allToDo.appendChild(Todos);
                                }
                                if(response.Status=="InProgress"){
                                    var inprogressTodo =generateNewTodo(response.Item,response.StatusId)
                                    inprogressTasks.appendChild(inprogressTodo);
                                }
                                if(response.Status=="Completed"){
                                    var completedTodo =generateNewTodo(response.Item,response.StatusId)
                                    completedTasks.appendChild(completedTodo);
                                }
                            })
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }  
function saveTodo(){
    uniqueId = Math.random().toString(16).slice(2);
    let item=newToDo.value;
    var data=JSON.stringify(
        {
            "Item":item,
            "Status":"Todo",
            "StatusId":uniqueId
        });
        $.ajax( {
            url: 'http://localhost:50110/api/TodoList/PostToDo',
            type: 'POST',
            headers: {
                Authorization : `Bearer ${token}`
            },
            data:data,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function( response ) {
                alert(response);
            },
            error: function (error) {
                alert(error);
            }
        });
}
