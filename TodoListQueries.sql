	
	--Databse name=Task
	--Table Name=TodoList
	use Task

	create table TodoList(Id int identity(1,1),Item varchar(100),Status varchar(20),StatusId varchar(50))
	
	create procedure post_Todo(@Item varchar(100),@Status varchar(20),@StatusId varchar(50))
	as begin 
	insert into TodoList values(@Item,@Status,@StatusId)
	end

	create procedure Delete_Todo(@StatusId varchar(50))
	as begin 
	delete from TodoList where StatusId=@StatusId
	end 

	create procedure Update_Todo(@StatusId varchar(50),@Item varchar(100))
	as begin 
	update TodoList set Item=@Item where StatusId=@StatusId
	end

	create procedure Get_All_Todos
	as begin 
	select Item,Status,StatusId from TodoList
	end

	create procedure Update_Status(@status varchar(20),@StatusId varchar(50))
	as begin 
	update TodoList set Status=@status where StatusId=@StatusId
	end 