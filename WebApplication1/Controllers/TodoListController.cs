﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class TodoListController : ApiController
    {
        [Authorize]
        [HttpGet]
        public DataTable GetAllTodos()
        {
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString());
            SqlCommand cmd = new SqlCommand("Get_All_Todos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            return dt;
        }
        [Authorize]
        [HttpPost]
        public HttpResponseMessage PostToDo(ToDoList todolist)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString());
                SqlCommand cmd = new SqlCommand("post_Todo", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Item", todolist.Item);
                cmd.Parameters.AddWithValue("@Status", todolist.Status);
                cmd.Parameters.AddWithValue("@StatusId", todolist.StatusId);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return Request.CreateResponse(HttpStatusCode.Created,"ToDo Created Successfully"); 
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        [Authorize]
        [HttpDelete]
        public HttpResponseMessage DeleteToDo(string id)
        {
            try { 
                DataTable dt = new DataTable();
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString());
                SqlCommand cmd = new SqlCommand("Delete_Todo", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StatusId",id );
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return Request.CreateResponse(HttpStatusCode.OK, "ToDo Deleted Successfully");          
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        [Authorize]
        [HttpPut]
        public HttpResponseMessage UpdateToDo(ToDoList todolist, string id)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString());
                SqlCommand cmd = new SqlCommand("Update_Todo", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StatusId", id);
                cmd.Parameters.AddWithValue("@Item", todolist.Item);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return Request.CreateResponse(HttpStatusCode.OK,"Todo Updated Successfully");
            }
            catch(Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,ex);
            }
        }
    }
}
