﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;


namespace WebApplication1.Controllers
{
    public class TodoStatusController : ApiController
    {
        [HttpGet]
        public object CreateToken()
        {
            string key = "Todo_List_Secret_Key_1122";
            var issuer = "http://localhost:50110";
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var getClaims = new List<Claim>();
            getClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            getClaims.Add(new Claim("Username", "Raja"));
            getClaims.Add(new Claim("Password", "Raja123"));

            var token = new JwtSecurityToken(
                issuer,
                issuer,
                getClaims,
                expires: DateTime.Now.AddDays(5),
                signingCredentials: credentials
                );
            var newToken = new JwtSecurityTokenHandler().WriteToken(token);
            return new { data = newToken };
        }
        [Authorize]
        [HttpPut]
        public HttpResponseMessage UpdateStatus(ToDoList todolist, string id)
        {
            try 
            { 
                DataTable dt = new DataTable();
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ToString());
                SqlCommand cmd = new SqlCommand("Update_Status", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StatusId", id);
                cmd.Parameters.AddWithValue("@Status", todolist.Status);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return Request.CreateResponse(HttpStatusCode.OK, "Todo Status Updated Successfully");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
