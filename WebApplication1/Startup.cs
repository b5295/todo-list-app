﻿using Microsoft.Owin;
using Owin;
using System;
using System.Threading.Tasks;

using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using Microsoft.IdentityModel.Tokens;
using System.Text;

[assembly: OwinStartup(typeof(WebApplication1.Startup))]

namespace WebApplication1
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888

            app.UseJwtBearerAuthentication(
               new JwtBearerAuthenticationOptions
               {
                   AuthenticationMode = AuthenticationMode.Active,
                   TokenValidationParameters = new TokenValidationParameters()
                   {
                       ValidateIssuer = true,
                       ValidateAudience = true,
                       ValidateIssuerSigningKey = true,
                       ValidIssuer = "http://localhost:50110",
                       ValidAudience = "http://localhost:50110",
                       IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("Todo_List_Secret_Key_1122"))
                   }
               });
        }
        
    }
}
