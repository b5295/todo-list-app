﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class ToDoList
    {
        public string Item { get; set; }
        public string Status { get; set; }
        public string StatusId { get; set; }
    }
}